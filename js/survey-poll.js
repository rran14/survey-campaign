// Define $ as jQuery;
!function ($) {

    // Make controllers and settings
    var settings = {
        controllers: {
            progressBar: "progress-bar", // get progress bar
            progressBarSteps: "progress-bar-step_", // put class according to progress
            surveyForm: "survey-campaign-form", // select parent of all slides for trasition effect
            surveyContainer: "survey-campaign-steps", // select parent of all slides for trasition effect
            slide: "fieldset", // all slides selector
            addSlideClass: "survey-campaign-slide", // add common class in all slides
            slideClass: "survey-campaign-step_", // add class in all slides,
            surveyItem: "survey-item", // Survey item
            slideHidden: "slide-hidden", // hidden class for slide
            btn: "data-display-slide", // button attribute for slide move (only arrtibute)
            errorClass: "error-message", // error message class
            directClick: "direct-link", // direct click
            notIncludedFields: ":not([disabled]):not([type='hidden']):not([type='submit']):not([type='button'])", // not included fields
            miniumOneSelection: "minium-one-selection", // user can select only one choice in 'controls' class 
            onlyOneSelection: "only-one-selection", // only one selection in 'controls' class
            btnSlidePrevious: "previous", // Previous slide
            btnSlideNext: "next", // Next slide
            btnStartAgain: "start-again", // start again
            surveySubmitBtn: "submit", // survey submit btn
            bodyClass: "survey-step_" // body class
        }
    };

    // Make class selector
    function makeClassSelector(element) {
        return "." + element;
    }

    // get old slide no.
    var oldSlideNo = 0;

    // Initilize funtion
    function init() {

        // Survey Submit
        $.fn.surveySubmit(makeClassSelector(settings.controllers.surveyForm));

        // put class in every slides
        $(settings.controllers.slide).each(function (i) {
            $(this).addClass(settings.controllers.addSlideClass + " " + settings.controllers.slideHidden + " " + settings.controllers.slideClass + i);
        });

        // remove hidden class in 1st slide
        $(makeClassSelector(settings.controllers.slideClass + "0")).removeClass(settings.controllers.slideHidden);

        // put body class 
        $("body").addClass(settings.controllers.bodyClass + "0");

        // put width and height in parent tag according to slides
        $(makeClassSelector(settings.controllers.surveyContainer)).css({ "height": $(makeClassSelector(settings.controllers.slideClass + "0")).outerHeight(), "width": $(settings.controllers.slide).outerWidth() * $(settings.controllers.slide).length });

        // Progress bar
        changeProgressBar(0)

        // activate Next Btn
        activateNextBtn($(makeClassSelector(settings.controllers.slideClass + "0")));

        // Hide previous and 'start again' btn on page load 
        disableBtn(settings.controllers.btnSlidePrevious, true);
    }

    //change progress bar
    function activateNextBtn(currentSlide) {
        var checkedItems = currentSlide.find("input:checked" + settings.controllers.notIncludedFields).length;
        if(currentSlide.has(makeClassSelector(settings.controllers.miniumOneSelection)).length && !checkedItems){
            disableBtn(settings.controllers.btnSlideNext, true);
        } else {
            disableBtn(settings.controllers.btnSlideNext, false);
        }
    }

    //change progress bar
    function changeProgressBar(index) {
        $(makeClassSelector(settings.controllers.progressBar)).removeClass(settings.controllers.progressBarSteps + oldSlideNo);
        $(makeClassSelector(settings.controllers.progressBar)).addClass(settings.controllers.progressBarSteps + index);
        
        // put body class 
        $("body").removeClass(settings.controllers.bodyClass + oldSlideNo);
        $("body").addClass(settings.controllers.bodyClass + index);
    }

    // Hide button function according to slide
    function disableBtn(ele, value) {
        if (value == true) {
            // previous and Start btn hide on 1st slide
            $("[" + settings.controllers.btn + "=" + ele + "]").addClass(settings.controllers.slideHidden);
        } else if (value == false) {
            // previous and Start btn show except 1st slide
            $("[" + settings.controllers.btn + "=" + ele + "]").removeClass(settings.controllers.slideHidden);
        }
    }

    // Validate current slide fields which is not disabled or hidden
    function validateCurrentSlide(currentSlide) {

        // Blur on each input
        $(currentSlide).find(" input" + settings.controllers.notIncludedFields + ", textarea" + settings.controllers.notIncludedFields + ", select" + settings.controllers.notIncludedFields).each(function () {
            $(this).trigger("blur");
        });

        // Check minimum one selection allowed in slide
        $(currentSlide).find(makeClassSelector(settings.controllers.miniumOneSelection)).each(function(){
            checkMiniumOneSelection($(this));
        })

        // Page scroll on error filed
        if ($(currentSlide).has(makeClassSelector(settings.controllers.errorClass)).length) {
            return false;
        } else {
            return true;
        }
    }

    // Check minium one selection in current slide
    function checkMiniumOneSelection(currentSlide) {

        // get checked item length
        var checkedItems = currentSlide.find("input:checked" + settings.controllers.notIncludedFields).length;

        if (checkedItems < 1) {
            if (!currentSlide.next(makeClassSelector(settings.controllers.errorClass)).length) {
                // if not checked any one the show error.
                currentSlide.addClass(settings.controllers.errorClass);
                currentSlide.after("<p class='"+settings.controllers.errorClass+"'>" + currentSlide.attr('data-errormsg') + "</p>");
            }
        } else {
            // Remove error if valid
            currentSlide.removeClass(settings.controllers.errorClass);
            currentSlide.next(makeClassSelector(settings.controllers.errorClass)).remove();
        }
    }

    // get slide number which need to display on click
    var previousClickedArray = [];
    function activeSlideRadioCheckbox(slideNo) {
        return $(makeClassSelector(settings.controllers.slideClass + slideNo)).find("input[type='checkbox']:checked,input[type='radio']:checked");
    }
    function checkSlideNo(self) {
        // If Current Slide No is 'not a number' , like 'previous, next and start again' btn
        if (isNaN(parseInt(self.getAttribute(settings.controllers.btn)))) {
            if (self.getAttribute(settings.controllers.btn) == settings.controllers.btnSlideNext) {
                var getNextScreenNo = activeSlideRadioCheckbox(oldSlideNo).first().attr(settings.controllers.btn);
                if(!getNextScreenNo){
                    getNextScreenNo = oldSlideNo + 1;
                }
                previousClickedArray.push({prevScreenNo:oldSlideNo,nextScreenNo:getNextScreenNo});
                return parseInt(getNextScreenNo);
            } else if (self.getAttribute(settings.controllers.btn) == settings.controllers.btnSlidePrevious) {
                var getPrevSlide = previousClickedArray.pop();
                activeSlideRadioCheckbox(parseInt(getPrevSlide.nextScreenNo)).parent().removeClass("checked");
                activeSlideRadioCheckbox(parseInt(getPrevSlide.nextScreenNo)).prop("checked", false);
                return parseInt(getPrevSlide.prevScreenNo);
            } else if (self.getAttribute(settings.controllers.btn) == settings.controllers.btnStartAgain) {
                return 0;
            } else if (self.getAttribute(settings.controllers.btn) == settings.controllers.surveySubmitBtn) {
                return $(settings.controllers.slide).length - 1;
            } else {
                return oldSlideNo;
            }
        } else if($(self).parent().hasClass(settings.controllers.directClick)) {// else return number which is passed on click
            previousClickedArray.push({prevScreenNo:oldSlideNo,nextScreenNo:parseInt(self.getAttribute(settings.controllers.btn))});
            return parseInt(self.getAttribute(settings.controllers.btn));
        } else {
            return oldSlideNo;
        }
    }

    // Check minium one selection in current slide
    function clickForSlideChange(self) {

        // if tag is not disabled
        if (!$(self).is(":disabled")) {

            // get slide number
            var currentSlideNo = checkSlideNo(self);

            // get current slide
            var currentSlide = $(makeClassSelector(settings.controllers.addSlideClass + ":not(" +makeClassSelector(settings.controllers.slideHidden)));

            // validate current slide except 'no validation' btn click
            if (currentSlideNo < oldSlideNo) {
                valid = true;
            } else {
                valid = validateCurrentSlide(currentSlide); // return boolean, current slide is valid or not.
            }

            // If slide is valid
            if (valid) {

                // Remove hidden class for display next slide
                $(makeClassSelector(settings.controllers.slideClass + currentSlideNo)).removeClass(settings.controllers.slideHidden);

                // put hidden class in current slide if curren and old slide are not same
                if (currentSlideNo != oldSlideNo) {
                    $(currentSlide).addClass(settings.controllers.slideHidden);
                    // Scroll up
                    $("html,body").animate({ scrollTop: $(makeClassSelector(settings.controllers.surveyContainer)).offset().top - $("#header").outerHeight() }, 100);
                }

                // Display progess bar 
                changeProgressBar(currentSlideNo)

                activateNextBtn($(makeClassSelector(settings.controllers.slideClass + currentSlideNo)));

                // previous , next and 'start again' btn show and hide according to slide count
                if (currentSlideNo == 0) {
                    disableBtn(settings.controllers.btnSlidePrevious, true);
                } else {
                    disableBtn(settings.controllers.btnSlidePrevious, false);
                }

                // scroll left and change height as per slide number
                $(makeClassSelector(settings.controllers.surveyContainer)).css({ "height": $(makeClassSelector(settings.controllers.slideClass + currentSlideNo)).outerHeight(), "left": "-" + $(settings.controllers.slide).outerWidth() * currentSlideNo + "px" });

                // get old slide no
                oldSlideNo = currentSlideNo;
            } else {
                // Height change for current slide
                $(makeClassSelector(settings.controllers.surveyContainer)).css({ "height": $(makeClassSelector(settings.controllers.slideClass + oldSlideNo)).outerHeight() });
                // Scroll up
                $('html, body').animate({scrollTop: $(currentSlide).find(makeClassSelector(settings.controllers.errorClass)).first().parents(".control-group").offset().top - $("#header").outerHeight()}, 100);
                $(currentSlide).find(makeClassSelector(settings.controllers.errorClass)).first().prev('input, select, textarea').focus();
            }
        }
    };

    // run functions after document ready
    $(document).ready(function () {

        // Initilize funtion
        init();

        // checkbox class click need to trigger change on checkbox
        $(makeClassSelector(settings.controllers.surveyForm)).find(".checkbox").on("click",function(){
            $(this).find("input[type='checkbox']").trigger("change");
        })

        // Click function on button or checkbox or radio field
        $("[" + settings.controllers.btn + "]:not(input[type='checkbox']):not(input[type='radio'])").on('click', function () {
            if(!$(this).is($("[" + settings.controllers.btn + "=" + settings.controllers.surveySubmitBtn + "]"))) {
                clickForSlideChange(this);
            }
        });

        // change function for select only one in current slide
        $(makeClassSelector(settings.controllers.surveyItem + " input[type='checkbox'] , input[type='radio']")).on('change', function () {
            if($(this).parents(makeClassSelector(settings.controllers.onlyOneSelection)).length && !$(this).is("checked")){
                clickForSlideChange(this);
                $(this).parents(makeClassSelector(settings.controllers.onlyOneSelection)).find("input[type='checkbox'] , input[type='radio']").prop("checked", false);
                $(this).parents(makeClassSelector(settings.controllers.onlyOneSelection)).find(".checked").removeClass("checked");
                $(this).prop("checked", true);
                $(this).parent().addClass("checked");
            } else{
                if($(this).is(":checked")) {
                    clickForSlideChange(this);
                    $(this).parent().addClass("checked");
                } else {
                    $(this).parent().removeClass("checked");
                }
            }
        });

        // change function for select only one in current slide
        $(makeClassSelector(settings.controllers.surveyForm + " input" + settings.controllers.notIncludedFields + ", textarea" + settings.controllers.notIncludedFields + ", select" + settings.controllers.notIncludedFields)).on('blur', function () {
            $(makeClassSelector(settings.controllers.surveyContainer)).css({ "height": $(makeClassSelector(settings.controllers.slideClass + oldSlideNo)).outerHeight() });
        });

    });
}(window.jQuery);