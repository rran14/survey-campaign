/* Form Validations */
/* =========================================================== */
! function ($) {
    "use strict"; // jshint ;_;

    var isValid = true;
    var patterns = {
        pass: /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[@'#.$;%^&+=!"()*,-/:<>?_]).{8,}$/,
        confpass: 'confpass',
        email: /^[\W\w]+@[\W\w]+\.[\W\w]{2,3}$/,
        twochars: /^.{2,}$/,
        fnamena: /^[^\s0-9][a-zA-Z,\.'\()\- ]{1,30}$/,
        zipcode: /^\d{5}$/,
        /*zipcode4: /^\d{4}$/,*/
        zipcode4: /^([a-zA-Z]{1}[\d]{4}[a-zA-Z]{3}|[\d]{4})$/,
        letter: /^[a-zA-Z]+$/,
        address1: /^.{2,50}$/,
        usPhoneNumber: /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/,
        euPhoneNumber: /^\d{8,11}$/,
        clphoneNumber: /^\d{6,15}$/,
        numeric: /^[0-9]+$/,
        prefix: /^[a-zA-Z0-9]+$/,
        name: /^[a-zA-Z0-9\s]+$/,
        zip59: /^(\d{5}(\d{4})?)?$/,
        usPhoneNumber1: /^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$/,
        alphanumeric6: /^([A-PR-UWYZ](([0-9](([0-9]|[A-HJKSTUW])?)?)|([A-HK-Y][0-9]([0-9]|[ABEHMNPRVWXY])?)) [0-9][ABD-HJLNP-UW-Z]{2})|GIR 0AA$/,
    };

    function isRequired(formElement) {

        // check if the data-validate property in HTML is set
        if (formElement.attr('data-validate') == 'required') {
            // if no value is set
            if (formElement.val() == '') {
                isValid = false;
                displayError(formElement);
            }
            // if set
            else {
                deleteError(formElement);
            }
        }
    }

    function displayError(formElement, noBorder, errorMsg) {
        //console.log(formElement)
        if (!formElement.hasClass('error')) {

            if (!noBorder)
                formElement.addClass('error');

            if (errorMsg)
                errorMsg = errorMsg;
            else if (formElement.attr('data-errormsg'))
                errorMsg = formElement.attr('data-errormsg');
            else if (!errorMsg)
                errorMsg = $('.error-msg').val();

            formElement.after('<p class="error-message">' + errorMsg + '</p>');
        }
    }

    function deleteError(formElement) {
        if (formElement.hasClass('error'))
            formElement.removeClass('error');

        formElement.next('.error-message').remove();
    }

    function isValidDate(month, day, year) {
        var daysOfMonthArray = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if ((!(year % 4) && year % 100) || !(year % 400)) {
            daysOfMonthArray[1] = 29;
        }
        return day <= daysOfMonthArray[--month];
    }

    function isValidPattern(formElement) {
        if (formElement.attr('data-pattern') && formElement.val().length > 0) {
            var patternValue = formElement.attr('data-pattern');

            if (patternValue == 'confpass') {
                patterns[patternValue] = new RegExp('^' + $('#password').val() + '$');
                //console.log(patterns[patternValue]);
            }
            console.log(formElement.val().match(patterns[patternValue]));
            if (formElement.val().match(patterns[patternValue]) === null) {
                isValid = false;
                displayError(formElement, '', formElement.attr('data-pattern-error'));
            } else {
                deleteError(formElement);
            }
        }
        else if (formElement.attr('data-pattern') && formElement.attr('data-validate') === undefined) {
            deleteError(formElement);
        }
    }

    function selectValidations(formElement) {

        formElement.find('.select-group').each(function (index, Element) {

            var validDate,
                month = 0,
                year = 0,
                day = 0,
                counter = 0,
                valueCounter = 0,
                isRequiredField = undefined;

            // check the value of each select box
            $(this).find('select').each(function (e) {
                if ($(this).attr('rel') == 'month') {
                    if ($(this).val() != "") {
                        month = $(this).val();
                        valueCounter++;
                    }
                    counter++;
                } else if ($(this).attr('rel') == 'year') {
                    if ($(this).val() != "") {
                        year = $(this).val();
                        valueCounter++;
                    }
                    counter++;
                } else {
                    if ($(this).val() != "") {
                        day = $(this).val();
                        valueCounter++;
                    }
                    counter++;
                }
            });

            isRequiredField = $(this).attr('data-validate');

            // check if the date is valid or not and display error
            if ((valueCounter > 0 && valueCounter < counter) || (isRequiredField == 'required' && valueCounter < counter)) {
                displayError($(this));
                isValid = false;
            } else if (valueCounter == counter) {

                deleteError($(this), 'noBorder');

                // validate date only if we have value of month,year,day
                if (month != '0' || day != '0' || year != '0') {
                    validDate = isValidDate(month, day, year);
                    if (!validDate) {
                        displayError($(this), 'noBorder');
                        isValid = false;
                    } else {
                        deleteError($(this), 'noBorder');
                        if (day == '0') day = 1;
                        $('#' + $(this).attr('rel')).val(year + '-' + month + '-' + day + 'T00:00:00Z');
                    }
                }
            } else {
                deleteError($(this), 'noBorder');
            }
        });
    }

    function checkValidation(formElement) {
        var msg = $("#term_error").val();

        formElement.find('.check-group input').each(function (index, Element) {
            $('div.check').find("p.error-message").remove();
            if ($(this).attr('data-validate') == 'required' && $(this).is(':checked') === false) {
                $(this).parents('div.check').append('<p class="error-message">' + msg + '</p>');
                isValid = false;
            }

        });
    }

    function limitCheckboxes(selectors) {
        $(selectors).change(function () {
            var group = ":checkbox[name='" + $(this).attr("name") + "']";
            if ($(this).is(':checked')) {
                $(group).not($(this)).attr("checked", false);
                //console.log($(group).not($(this)).attr("checked",false))
            }
            fillOptInValue($(this).attr('id'));
        });
    }

    var fillOptInValue = function (src) {
        var brand = 'false',
            corporate = 'false';

        if ($('#brand').is(':checked')) {
            brand = 'true';
        };
        if ($('#corporate').is(':checked')) {
            corporate = 'true';
        };

        switch (src) {
            case 'brand':
                $('#optInbrand').val(brand);
                $('#optIncorporate').val('false');
                $('#corporate').parent('span').removeClass('checked');
                break;
            case 'corporate':
                $('#optIncorporate').val(corporate);
                $('#optInbrand').val('false');
                $('#brand').parent('span').removeClass('checked');
                break;
            default:
                break;
        }
    };

    var limitCheckbox = limitCheckboxes('#brand:checkbox, #corporate:checkbox');
    // setting the hidden value to the input
    $('.formDateTime').val(JSON.parse(JSON.stringify(new Date())));

    $('form input, form textarea, form select').blur(function (index, Element) {
        isRequired($(this));
        isValidPattern($(this));
    });

    function validateForm(selector) {
        isValid = true;

        selector.find('input, textarea, select').each(function (index, Element) {
            isRequired($(this));
            if (isValid) {
                isValidPattern($(this));
            }

        });

        selectValidations(selector);
        checkValidation(selector);

        return isValid;
    }


    $('.contact-submit-btn').click(function (index, Element) {
        var isFormValid = validateForm($(this).parents('form'));
        if (isFormValid) {
            var request = UFORM.postData('contactform', genericFormServiceUrl,
                function (result) {
                    //alert(result);
                    var redirect = $('#redirectPage').val();
                    if ($('#referFriend').length > 0) {
                        var objForm = $('#contactform');
                        var selArray = '';
                        var submitForm = $('<form>', {
                            'action': redirect,
                            'target': '_top',
                            'method': 'POST'
                        });

                        selArray = objForm.serializeArray();
                        $.each(selArray, function (key, val) {
                            if (val.name == 'name') {
                                val.name = 'senderName';
                            }
                            if (val.name == "message") {
                                val.value = escape(val.value);
                            }
                            var input = $('<input/>', {
                                'name': val.name,
                                'value': val.value,
                                'type': 'hidden'
                            });
                            submitForm.append(input);
                        });

                        submitForm.appendTo('body');
                        submitForm.submit();
                    } else {
                        window.location.href = redirect;
                    }
                    // do something with result data
                },
                function (jqXhr, textStatus) {
                    // alert(textStatus);
                    var obj = jQuery.parseJSON(jqXhr.responseText);
                    deleteError($('#recaptcha_div'));
                    if (obj && obj.Errors) {
                        for (var key in obj.Errors) {
                            $('#divMsg').html(obj.Errors[key].Value);
                            displayError($('#recaptcha_div'), 'noBorder', obj.Errors[key].Value);
                        }
                        show_recaptcha(racptachObj.public_key, racptachObj.lang);
                    } else {
                        // alert(jqXHR.responseText);
                        displayError($('#recaptcha_div'), 'noBorder', jqXhr.responseText);
                    }
                    // do something with failure data
                });

            return true
        }
        else {
            $('html, body').animate({
                scrollTop: $(this).parents('form').find('.error-message').offset().top - 200
            }, 800);
            return false
        }
    });

    $('form').submit(function (index, Element) {
        var isFormValid = validateForm($(this));
        if (isFormValid === false) {
            $('html, body').animate({
                scrollTop: $(this).find('.error-message').offset().top - 100
            }, 800);
            return false
        }
    });


    var isValidCheckbox = false;
    
    // Check minium one selection in current slide
    function checkMiniumOneSelection(selectParent) {

        // get checked item length
        let checkedItems = selectParent.find("input:checked:not([disabled]):not([type='hidden']):not([type='submit']):not([type='button'])").length;

        if (checkedItems < 1) {
            if (!selectParent.next(".error-message").length) {
                // if not checked any one the show error.
                selectParent.addClass("error-message");
                selectParent.after("<p class='error-message'>" + selectParent.attr('data-errormsg') + "</p>");
            }
            isValidCheckbox = false;
        } else {
            // Remove error if valid
            selectParent.removeClass("error-message");
            selectParent.next(".error-message").remove();
            isValidCheckbox = true;
        }
    }


    // start survey form submit
    $.fn.surveySubmit = function (element) {
        $(element + " input:not([disabled]):not([type='hidden']):not([type='submit']):not([type='button']), textarea:not([disabled]):not([type='hidden']):not([type='submit']):not([type='button']), select:not([disabled]):not([type='hidden']):not([type='submit']):not([type='button'])").blur(function () {
            isRequired($(this));
            isValidPattern($(this));
            selectValidations($(this).closest('.select-group').parent());
        });
        //survey submit handler
        $(element + " .survey-submit").click(function () {

            $(this).parents('form').find('.survey-campaign-slide:not(.slide-hidden) .minium-one-selection').each(function(){
                checkMiniumOneSelection($(this));
            })


            let isValid = validateForm($(this).parents('form'));

            if(isValidCheckbox && isValid) {
                isValid = true;
            } else {
                isValid = false
            }
            if (isValid === true) {

                // passing date of birth value 
                if ($(element + " #birthdayMonth").val() !== '' && $(element + " #birthdayDay").val() !== '' && $(element + " #birthdayYear").val() !== '') {
                    $(this).parents('form').find('input[type=hidden]').each(function () {
                        if ($(this).attr('name') == 'contact.birthday') {
                            var month = $(element + " #birthdayMonth").val();
                            var day = $(element + " #birthdayDay").val();
                            var year = $(element + " #birthdayYear").val();
                            $(this).val(year + '-' + month + '-' + day + 'T00:00:00Z');
                            return false;
                        }
                    });
                }
                // Conditions Handled for select Question for DIRTY FIXED .
                var arSelectedQuestionAnswerId = new Array();
                $(element + " .survey-item").each(function (index) {
                    var _this = $(this);
                    $(this).find('input[type=hidden]').attr('disabled', 'disabled');
                    $(this).find('input[type=radio]').each(function (index) {
                        if ($(this).attr('checked')) {
                            $(_this).find('.surveyQuestionId').removeAttr('disabled');
                            $(_this).find('.surveyQuestionText').removeAttr('disabled');
                            $(this).parent().next('input[type=hidden]').removeAttr('disabled');
                            arSelectedQuestionAnswerId.push($(this).parent().next('input[type=hidden]').attr('name'));
                        }
                    });
                    $(this).find('input[type="checkbox"]').each(function (index) {
                        if ($(this).attr('checked')) {
                            $(_this).find('.surveyQuestionId').removeAttr('disabled');
                            $(_this).find('.surveyQuestionText').removeAttr('disabled');
                            $(this).parent().siblings('input[type=hidden]').removeAttr('disabled');
                            arSelectedQuestionAnswerId.push($(this).parent().siblings('input[type=hidden]').attr('name'));
                        }
                    });
                    $(this).find('select').each(function (index) {
                        if ($(this).val() !== "") {
                            $(_this).find('.surveyQuestionId').removeAttr('disabled');
                            $(_this).find('.surveyQuestionText').removeAttr('disabled');
                            $(this).parent().next('input[type=hidden]').removeAttr('disabled');
                            $(this).parent().next().val($(this).children(':selected').attr('data-answer-id'));
                            arSelectedQuestionAnswerId.push($(this).parent().next('input[type=hidden]').attr('name'));
                        }
                    });
                    // Append Question ID's form name those need to be passed in api_number along with selected Answer ID's form element name.
                    if (!$(_this).find('.surveyQuestionId').attr('disabled')) {
                        arSelectedQuestionAnswerId.push($(_this).find('.surveyQuestionId').attr('name'));
                    }
                });

                if (arSelectedQuestionAnswerId.length > 0) {
                    // From element name of selected question id's and answer id's are required to passed in element with name api_number.
                    $(element + " #api_number").val(arSelectedQuestionAnswerId.toString());
                } else {
                    $(element + " #api_number").remove();
                    $(element + " #surveyResponseList").remove(); //survey id node will go in json only when any survey ques is answered by user
                }

                $(element + " .ajax-loader").show();
                var FormId = $(this).parents('form').attr('id');

                var request = UFORM.postData(FormId, genericFormServiceUrl,
                    function (result) {
                        if ($(element + " #corporate").is(':checked')) {
                            UDM.evq.push(['trackEvent', 'Conversion', 'Acquisition: Mktg Opt ins', 'Corporate Opt-In']);
                        }
                        var redirect = $(element + " #redirectPage").val();
                        window.location.href = redirect;
                        // do something with result data
                    },
                    function (jqXhr, textStatus) {
                        // alert(textStatus);
                        var obj = jQuery.parseJSON(jqXhr.responseText);
                        $(element + " .ajax-loader").hide();
                        if (obj && obj.Errors) {
                            for (var key in obj.Errors) {
                                var pointer;
                                $(element + " #divMsg").html(obj.Errors[key].Value);
                                if (!$(element + " #cookielessFlag")) {
                                    pointer = element + " #recaptcha_div";
                                } else {
                                    pointer = element + " #cl-response-field";
                                    $(element + " .o-btn--reset-captcha").trigger('click');
                                };

                                if ($(pointer).next('.error').length === 0) {
                                    $(pointer).after('<span class="error label label-important">' + obj.Errors[key].Value + '</span>');
                                } else {
                                    $(pointer).next('.error').remove('.error');
                                    $(pointer).after('<span class="error label label-important">' + obj.Errors[key].Value + '</span>');
                                }
                            }
                        }
                        if (!$(element + " #cookielessFlag")) {
                            show_recaptcha(racptachObj.public_key, racptachObj.lang);
                        }

                        else {
                            if ($(element + " #recaptcha_div").next('.error').length === 0) {
                                $(element + " #recaptcha_div").after('<span class="error label label-important">' + jqXhr.responseText + '</span>');
                            } else {
                                $(element + " #recaptcha_div").next('.error').remove('.error');
                                $(element + " #recaptcha_div").after('<span class="error label label-important">' + jqXhr.responseText + '</span>');
                            }
                        }
                        $(".survey-campaign-steps").css({ "height": $(".survey-campaign-slide:not(.slide-hidden)").outerHeight() });
                        // do something with failure data
                    });

                    $(".survey-campaign-steps").css({ "height": $(".survey-campaign-slide:not(.slide-hidden)").outerHeight() });

                return true;
            } else {

                $(".survey-campaign-steps").css({ "height": $(".survey-campaign-slide:not(.slide-hidden)").outerHeight() });
                $('html, body').animate({
                    scrollTop: $(element).find('p.error-message').first().parents(".control-group").offset().top - $("#header").outerHeight()
                }, 800);
                $(element).find('p.error-message').first().prev('input, select, textarea').focus();
                return false;
            }
        });
    }
    // end survey form submit

}(window.jQuery);