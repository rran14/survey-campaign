<?php
/**
 * Template Name: Survey Campaign Landing Page
 */
define('DONOTCACHEPAGE', true);
global $body_class, $post;
$page_name = 'survey-campaign';
$body_class = 'survey-campaign';
global $page_id;
$page_id = 'survey-campaign';
get_header();

wp_enqueue_script('survey-js', get_stylesheet_directory_uri() . '/js/survey-poll-min.js');
wp_enqueue_style('survey-style', get_stylesheet_directory_uri() . '/css/survey-poll-min.css');
?>
<script type='text/javascript'>
    /* <![CDATA[ */
    var racptachObj = {"lang": "nl", "public_key": "6Ld8Xb8SAAAAAEu2TCCW3sPQvlnuVsqASdRCG9Vu"};
    /* ]]> */
</script>
<section id="main" role="main">
        <div class="container">
        <h1 class="hide"><?php echo $post->post_title; ?></h1>
        <div class="content-section"><?php echo $post->post_content ?></div>
    <?php if ($_GET['submit'] == 'success') {  ?>
            <section class="survey-campaign-section black">
                <article class="register-thanks-cont">
                    <h2><?php _e('Thank you very much for participating!', LANGUAGE_DOMAIN_NAME) ?></h2>
                    <div class="thankyou">
                        <p><a href="<?php echo home_url('/'); ?>" title="<?php _e('I continued browsing our web.', LANGUAGE_DOMAIN_NAME) ?>"><?php _e('I continued browsing our web.', LANGUAGE_DOMAIN_NAME) ?></a></p>
                    </div>

                </article>

            </section>
    <?php } else { ?>
        <section class="survey-campaign-section">
            <form id="promoEvolution" class="survey-campaign-form"  name="promoEvolution" action="" method="post">
                <div class="ajax-loader"><img src="<?php echo get_template_directory_uri() ?>/img/icons/ajax-loader.gif"/ alt="ajax loader"></div>
                <?php global $post_ID; ?>
                <input id ="redirectPage" type="hidden" value="<?php echo (get_permalink($post_ID) . '?submit=success' ) ?>"/>
                <input type="hidden" class="error-msg" value="<?php _e("Please fill out this field.", LANGUAGE_DOMAIN_NAME) ?>" />                
                <?php wp_nonce_field('survey-campaign','survey_nonce'); ?>
                <?php
                $theme_path = get_stylesheet_directory() . '/';
                $obParse = new ParseContactXml();
                $obParse->parserIntialise($theme_path . 'SurveyCampaign.xml');
                ?>
            </form>
        </section>
    <?php } ?>
    </div>
</section>

<?php get_footer(); ?>
