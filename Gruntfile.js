module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                files: {
                    'css/survey-poll.css' : 'sass/survey-poll.scss'
                }
            },
            options: {
                sourceMap: false, // Create source map
            },
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass']
            }
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                'css/survey-poll-min.css': ['css/survey-poll.css']
                }
            }
        },
        uglify: {
            options: {
              mangle: false
            },
            my_target: {
              files: {
                'js/survey-poll-min.js': ['js/survey-poll.js']
              }
            }
          }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default',['sass','watch']);
    grunt.registerTask('build', ['cssmin','uglify']);
}