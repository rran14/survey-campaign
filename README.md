**Grunt functions written for below tasks**
1. Sass to css convert and run watch command (Run - grunt)
2. css and js minify (Run - grunt build)


**Form submit function written in formvalidation.js**
Form submit with survey quextion and captcha code error.


**Survey functions are written in survey-poll.js where below functionlity done**
1. hide 'start again, next, previous and submit' btn accoring to screen.
2. progress bar accoring to screen no.
3. get slide number to display through 'data-display-slide'
4. put 'directClick' class to move directly to the slide after click on 'checkbox or radio' btn through 'data-display-slide' attribute
5. put 'minium-one-selection' in Rowgroup for select atleast one 'checkbox or radio' btn
6. put 'only-one-selection' in Rowgroup for select only one selection of 'checkbox or radio' btn
7. page scroll when display 1st error or top position of section in any screen change
